import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {MatSliderModule} from "@angular/material/slider"
import {MatTableModule} from "@angular/material/table"
import {MatInputModule} from "@angular/material/input"
import {ReactiveFormsModule} from "@angular/forms"
import { MatFormFieldModule } from "@angular/material/form-field"
import { MatButtonModule } from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';

const modules = [
    MatTableModule,
    MatSliderModule,
    MatInputModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule
]

@NgModule({
    declarations: [],
    imports: [
        CommonModule,
        ...modules,
    ],
    exports: [
        ...modules
    ]
})

export class UtilModule {}

