import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddUserComponent } from './add-user/add-user.component';
import { AltaComponent } from './alta/alta.component';
import { DetailsComponent } from './details/details.component';
import { EditarComponent } from './editar/editar.component';
import { ListComponent } from './list/list.component';


const routes: Routes = [{
  path: 'list', component: ListComponent,
}, 
{
  path: 'add-user', component: AddUserComponent,
},

{
  path: 'details', component: DetailsComponent,
},

{
  path: 'alta', component: AltaComponent,
},

{
  path: 'editar/:id', component: EditarComponent,
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UsersRoutingModule { }
