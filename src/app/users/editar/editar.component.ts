import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Route, Router } from '@angular/router';

import {User} from "../models/user.model"
import { UserService } from '../user.service';

@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {

  formulario:FormGroup = new FormGroup ({
    "id": new FormControl(),
    "name": new FormControl(),
    "username": new FormControl(),
    "phone": new FormControl(),
  })

  id!: number


  constructor(private userService: UserService, 
              private _router: ActivatedRoute,
              private _route: Router) { }

  ngOnInit(): void {
    this.id = this._router.snapshot.params["id"];
    this.getUser()   
  }
  
  getUser(){

    if (Number(this.id)) {        
      this.userService.getUser(this.id).subscribe({
        next: (x) => {             
          this.formulario.controls.id.patchValue(x.id);
          this.formulario.controls.id.disable();
          this.formulario.controls.name.patchValue(x.name);
          this.formulario.controls.username.patchValue(x.username);
          this.formulario.controls.phone.patchValue(x.phone);
        },
        error: (err) => alert("hubo un problema al obtener el usuario") 
      })      
    } else {
      alert("Id invalido")
    } 
  }

  editarUsuario() {   
    
    if(this.formulario.valid){ 
      let user = this.formulario.value
      this.userService.editUser(this.id ,user).subscribe({
        next: (x) => {
          alert(`Se actualizaron los datos de ${user.username}`);
          console.log(x)
        }, error: (err) => {
          alert("ERROR: No se pudo modificar al usuario")
          console.log(err)
        },
        complete: () => this._route.navigate(["/users/list"])
      });
      
    } else { 
      alert("Faltan campos por completar")
    }
  } 
}
