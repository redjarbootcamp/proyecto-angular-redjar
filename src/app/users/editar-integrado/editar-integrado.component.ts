import { Component, OnInit, Input, Output, EventEmitter, SimpleChanges, OnChanges } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { User } from '../models/user.model';
import { UserService } from '../user.service';


@Component({
  selector: 'app-editar-integrado',
  templateUrl: './editar-integrado.component.html',
  styleUrls: ['./editar-integrado.component.css']
})
export class EditarIntegradoComponent implements OnChanges {

  constructor(private _userService:UserService) { }

  @Input() user!: User 
  @Input() id!: number
  
  @Output() fav = new EventEmitter<User>()

  formulario:FormGroup = new FormGroup ({
    "id": new FormControl(),
    "name": new FormControl(),
    "username": new FormControl(),
    "phone": new FormControl(),
  })

    ngOnChanges(changes:SimpleChanges): void { 
    if (!(changes.user.firstChange)){
      this.formulario.controls.id.patchValue(this.user.id);
      this.formulario.controls.id.disable();
      this.formulario.controls.name.patchValue(this.user.name);
      this.formulario.controls.name.enable();
      this.formulario.controls.username.patchValue(this.user.username);
      this.formulario.controls.username.enable();
      this.formulario.controls.phone.patchValue(this.user.phone);
      this.formulario.controls.phone.enable();
    } else {
      this.formulario.controls.id.disable();
      this.formulario.controls.name.disable();
      this.formulario.controls.username.disable();
      this.formulario.controls.phone.disable();
    }  
  }    

  editarUsuario() {   

    if(this.formulario.valid){ 
      let user: User = this.formulario.value
      this._userService.editUser(this.id ,user).subscribe({
        next: (x) => {
          alert(`Se actualizaron los datos de ${user.username}`);
          console.log(x)
        }, error: (err) => { 
          alert("ERROR: No se pudo modificar al usuario")          
          console.log(err)
        },     
        complete: () => {}
      });
      
    } else { 
      alert("Faltan campos por completar")
    }
  }

  enviarFavorito(){    
    this.fav.emit(this.user)    
  }
}
