import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarIntegradoComponent } from './editar-integrado.component';

describe('EditarIntegradoComponent', () => {
  let component: EditarIntegradoComponent;
  let fixture: ComponentFixture<EditarIntegradoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditarIntegradoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarIntegradoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
