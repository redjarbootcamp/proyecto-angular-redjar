import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from './models/user.model';
import { userDetails } from './models/userDetails';
import { NumberSymbol } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  userDetails!: userDetails
  favorites: User[] = []
  
  constructor(private _httpclient: HttpClient) { }

  getusers():Observable<User[]>{
    return this._httpclient.get<User[]>('https://jsonplaceholder.typicode.com/users')
  }

  getUser(id: number): Observable<User>{
    return this._httpclient.get<User>('https://jsonplaceholder.typicode.com/users/' + id)
  }

  postUser(user: User): Observable<User>{
    return this._httpclient.post<User>('https://jsonplaceholder.typicode.com/users/', user)
  }

  // Consultar metodo!!!
  editUser(id:number, user: User): Observable<User>{
    return this._httpclient.put<User>('https://jsonplaceholder.typicode.com/users/' + id, user)
  }

  // Consultar metodo!!!
  deleteUser(id:number): Observable<User>{
    return this._httpclient.delete<User>('https://jsonplaceholder.typicode.com/users/' + id)
  }

  getUserDetails(): userDetails{
    return this.userDetails
  }

  setUserDetails(userDetails:userDetails){
    this.userDetails = userDetails
  }

  getFavorites(): User[] {
    return this.favorites
  }

  setFavorites(fav:User){
    this.favorites = [...this.favorites, fav]
  }

  removeFavorite(id:number){
    this.favorites = this.favorites.filter(x => x.id != id)
  }
}
