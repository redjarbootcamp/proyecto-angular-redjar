export interface userDetails {
    id: number,
    name: string,
    username: string,
    phone: string,
    address: {
        street: string,
        suite: string,
        city: string,
        zipcode: string,
    },    
    website: string
}