import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import {User} from "../models/user.model"
import { UserService } from '../user.service';

@Component({
  selector: 'app-alta',
  templateUrl: './alta.component.html',
  styleUrls: ['./alta.component.css']
})
export class AltaComponent implements OnInit {
  formulario:FormGroup = new FormGroup ({
    "id": new FormControl(),
    "name": new FormControl(),
    "username": new FormControl(),
    "phone": new FormControl()
  })

  user: User = {} as User
  id!:number

  constructor(private userService: UserService) { }

  ngOnInit(): void {
  }

  nuevoUsuario(){
    if(this.formulario.valid){
      this.user.id = this.formulario.controls["id"].value;
      this.user.name = this.formulario.controls["name"].value;
      this.user.username = this.formulario.controls["username"].value;
      this.user.phone = this.formulario.controls["phone"].value;
      this.userService.postUser(this.user).subscribe({
        next: (x) => {
          alert(`Se agregoe al usuario: ${this.user.username}`)
          console.log(x);
        }, 
        error: (err) => {
          alert(`No se pudo agregar al usuario`)
          console.log(err)
        },
        complete: () => {}
      });
      
    } else { 
      console.log("Faltan campos por completar")
    }
  }
}
