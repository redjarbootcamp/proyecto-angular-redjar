import { Component, OnInit} from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../models/user.model';
import { userDetails } from '../models/userDetails';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  listUsers: User[] = []
  listaFavoritos: User[] = []

  displayedColumsUsers: string[] = ['id', 'name', 'username', 'phone', 'editar']
  displayedColumsFav: string[] = ['id' ,'username', 'quitar']

  user!: User 
  userDetails!: userDetails
  id!: number
  
  constructor(private userService: UserService, private _router: Router) { }

  ngOnInit(): void {
    this.getUsers()
    this.listaFavoritos = this.userService.getFavorites()
  }

  getUsers(){
    this.userService.getusers().subscribe({
      next: (x) => {  
        this.listUsers=x;
      } 
    })
  }

  irEditar(id:number) { 
    this._router.navigate([`/users/editar/${id}`])
  }
  
  eliminar(id:number){
    alert("Desea eliminar al usuario: " + this.listUsers[id-1].username) 
    this.userService.deleteUser(id).subscribe({
      next: () => {                
        let eliminado = this.listUsers.find(x => x.id === id)?.username;
        this.listUsers = this.listUsers.filter(x => x.id != id);               
        this.getUsers();
        this.userService.removeFavorite(id)
        this.listaFavoritos = this.userService.getFavorites()            
        alert("Se elimino a: " + eliminado)        
      },      
      error: () => {
        alert("No se pudo eliminar el usuario")  
      }           
    })    
  }

  editar(id: number, user: User){    
    this.id = id
    this.user = user
  }

  detalles(userDetails:userDetails){
    this.userService.setUserDetails(userDetails)
    this._router.navigateByUrl('/users/details')
  }

  agregarFavorito(fav:User){
    if(!this.user){
      alert("Seleccione un usuario")
    } else if (this.userService.getFavorites().includes(fav)) {
      alert("El usuario ya se encuentra en la lista de favoritos")
    } else {     
      this.userService.setFavorites(fav)
      this.listaFavoritos = this.userService.getFavorites()   
    }   
  }

  quitarFavorito(id:number){
    this.userService.removeFavorite(id)
    this.listaFavoritos = this.userService.getFavorites()
  }
}
