import { Component, Input, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { userDetails } from '../models/userDetails';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})

export class DetailsComponent implements OnInit {
  
  userDetails!: userDetails

  formulario: FormGroup = new FormGroup({
    "id": new FormControl(),
    "name": new FormControl(),
    "username": new FormControl(),
    "phone": new FormControl(),
    "street": new FormControl(),
    "suite": new FormControl(),
    "city": new FormControl(),
    "zipcode": new FormControl(),
    "website": new FormControl(),
  })

  constructor(private _userService: UserService) { }

  ngOnInit(): void {    
    
    this.userDetails = this._userService.getUserDetails()

    this.formulario.controls.id.patchValue(this.userDetails.id);
    this.formulario.controls.id.disable();
    this.formulario.controls.name.patchValue(this.userDetails.name);
    this.formulario.controls.name.disable();
    this.formulario.controls.username.patchValue(this.userDetails.username);
    this.formulario.controls.username.disable();
    this.formulario.controls.phone.patchValue(this.userDetails.phone);
    this.formulario.controls.phone.disable();
    this.formulario.controls.street.patchValue(this.userDetails.address.street);
    this.formulario.controls.street.disable();
    this.formulario.controls.suite.patchValue(this.userDetails.address.suite);
    this.formulario.controls.suite.disable();
    this.formulario.controls.city.patchValue(this.userDetails.address.city);
    this.formulario.controls.city.disable();
    this.formulario.controls.zipcode.patchValue(this.userDetails.address.zipcode);
    this.formulario.controls.zipcode.disable();
    this.formulario.controls.website.patchValue(this.userDetails.website);
    this.formulario.controls.website.disable();
  }
}
