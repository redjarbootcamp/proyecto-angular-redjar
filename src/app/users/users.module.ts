import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutingModule } from './users-routing.module';
import { ListComponent } from './list/list.component';
import { AddUserComponent } from './add-user/add-user.component';
import { HttpClientModule } from '@angular/common/http';
import { UserService } from './user.service';
import { DetailsComponent } from './details/details.component';
import { UtilModule } from '../Utils/utils.module';
import { AltaComponent } from './alta/alta.component';
import { EditarComponent } from './editar/editar.component';
import { EditarIntegradoComponent } from './editar-integrado/editar-integrado.component';




@NgModule({
  declarations: [
    ListComponent,
    AddUserComponent,
    DetailsComponent,
    AltaComponent,
    EditarComponent,
    EditarIntegradoComponent

  ],
  imports: [
    CommonModule,
    UsersRoutingModule,
    HttpClientModule,
    UtilModule
  ],
  providers: [
    UserService
  ]
})
export class UsersModule { }
