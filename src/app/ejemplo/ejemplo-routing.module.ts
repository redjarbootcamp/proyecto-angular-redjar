import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ComponenteEjemploComponent } from './componente-ejemplo/componente-ejemplo.component';
import { OtrosComponent } from './otros/otros.component';

const routes: Routes = [

  {
    path: "ejemplo", component: ComponenteEjemploComponent
  },
  {
    path: "otros", component: OtrosComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EjemploRoutingModule { }
