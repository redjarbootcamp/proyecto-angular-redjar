import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EjemploRoutingModule } from './ejemplo-routing.module';
import { ComponenteEjemploComponent } from './componente-ejemplo/componente-ejemplo.component';
import { OtrosComponent } from './otros/otros.component';


@NgModule({
  declarations: [
    ComponenteEjemploComponent,
    OtrosComponent
  ],
  imports: [
    CommonModule,
    EjemploRoutingModule    
  ],
})
export class EjemploModule { }
