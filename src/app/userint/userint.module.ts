import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UserintRoutingModule } from './userint-routing.module';
import { ViewlistComponent } from './viewlist/viewlist.component';
import { DetailsviewComponent } from './detailsview/detailsview.component';


@NgModule({
  declarations: [
    ViewlistComponent,
    DetailsviewComponent
  ],
  imports: [
    CommonModule,
    UserintRoutingModule
  ]
})
export class UserintModule { }
