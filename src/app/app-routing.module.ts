import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "users",
    loadChildren: () => import("../app/users/users.module").then(x => x.UsersModule)
  },

  {
    path: "ejemplo",
    loadChildren: () => import("../app/ejemplo/ejemplo.module").then(x => x.EjemploModule)
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
